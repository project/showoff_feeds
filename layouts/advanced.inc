<style>
 #showoff-feeds-advanced-layout {
   width: 100%;
   height: 100%;
   z-index: 100;
   position: absolute;
   top: 0px;
   left: 0px;
   background-color: #000;
   color: #fff;
 }
 #feed-1, #feed-2 {
   width: 50%;
   height: 100%;
   padding: 0px;
   float: left;
 }
</style>
<div id="showoff-feeds-advanced-layout" class="layout">
  <div id="feed-1" class="region"></div>
  <div id="feed-2" class="region"></div>
</div>