<?php
/**
 * @file
 * showoff_feeds.features.inc
 */

/**
 * Implements hook_views_api().
 */
function showoff_feeds_views_api() {
  return array("api" => "3.0");
}
